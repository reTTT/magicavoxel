extends Spatial

var items = [
	"res://vox/anim/deer.vox",
	"res://vox/anim/horse.vox",
	"res://vox/anim/T-Rex.vox",
	
	"res://vox/character/chr_bow.vox",
	"res://vox/character/chr_cat.vox",
	"res://vox/character/chr_fox.vox",
	"res://vox/character/chr_gumi.vox",
	"res://vox/character/chr_jp.vox",
	"res://vox/character/chr_knight.vox",
	"res://vox/character/chr_man.vox",
	"res://vox/character/chr_mom.vox",
	"res://vox/character/chr_old.vox",
	"res://vox/character/chr_poem.vox",
	"res://vox/character/chr_rain.vox",
	"res://vox/character/chr_sasami.vox",
	"res://vox/character/chr_sol.vox",
	"res://vox/character/chr_sword.vox",
	"res://vox/character/chr_tale.vox",
	"res://vox/character/chr_tama.vox",
	"res://vox/character/chr_tsurugi.vox",
#	"res://vox/character/",

	"res://vox/monument/monu0.vox",
	"res://vox/monument/monu1.vox",
	"res://vox/monument/monu2.vox",
	"res://vox/monument/monu3.vox",
	"res://vox/monument/monu4.vox",
	"res://vox/monument/monu5.vox",
	"res://vox/monument/monu6.vox",
	"res://vox/monument/monu7.vox",
	"res://vox/monument/monu8.vox",
	"res://vox/monument/monu9.vox",
	"res://vox/monument/monu10.vox",
	"res://vox/monument/monu16.vox",
#	"res://vox/monument/monu0.vox",
#	"res://vox/monument/monu0.vox",
]

var select_idx = -1
var select
var loading

# Member variables
var thread = Thread.new()


func _mv_load(path):
	var mesh = load("res://script/magicavoxel.gd").new()
	
#	mesh.set_script(script)
	if mesh._read_model_file(path):
		mesh._create_voxels()
	call_deferred("_ml_load_done")
	return mesh # return it

func _ml_load_done():
	# Wait for the thread to complete, get the returned value
	var mesh = thread.wait_to_finish()
	# Set to the sprite
	var poduim = get_node("podium")
	for c in poduim.get_children():
		c.queue_free()

	poduim.add_child(mesh)
	var max_size = 40
	var ss = mesh.size
	var scale = max_size / max(ss.x, max(ss.y, ss.z))
	mesh.set_scale(Vector3(scale, scale, scale))
	
	select.set_disabled(false)
	loading.hide()
	
func _on_load_pressed():
	if (thread.is_active()):
		# Already working
		return

	print("START THREAD!")
	#thread.start(self, "_mv_load", "res://temp/voxel-model-master/vox/monument/monu6-without-water.vox")
	thread.start(self, "_mv_load", items[select_idx])

func _ready():
	loading = get_node("../ui/loading")
	loading.hide()
	select = get_node("../ui/select")
	for i in range(items.size()):
		var name = items[i].get_file()
		select.add_item(name, i)
	
	select_idx = -1
	select.select(items.size()-2)
	_on_select_item_selected(items.size()-2)
	#_mv_load("res://temp/voxel-model-master/vox/monument/monu6-without-water.vox")
	#_on_load_pressed()


func _on_select_item_selected( ID ):
	select_idx = ID
	select.set_disabled(true)
	loading.show()
	_on_load_pressed()
