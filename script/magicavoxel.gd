extends MeshInstance

var mv_default_palette = [
	0x00000000, 0xffffffff, 0xffccffff, 0xff99ffff, 0xff66ffff, 0xff33ffff, 0xff00ffff, 0xffffccff, 0xffccccff, 0xff99ccff, 0xff66ccff, 0xff33ccff, 0xff00ccff, 0xffff99ff, 0xffcc99ff, 0xff9999ff,
	0xff6699ff, 0xff3399ff, 0xff0099ff, 0xffff66ff, 0xffcc66ff, 0xff9966ff, 0xff6666ff, 0xff3366ff, 0xff0066ff, 0xffff33ff, 0xffcc33ff, 0xff9933ff, 0xff6633ff, 0xff3333ff, 0xff0033ff, 0xffff00ff,
	0xffcc00ff, 0xff9900ff, 0xff6600ff, 0xff3300ff, 0xff0000ff, 0xffffffcc, 0xffccffcc, 0xff99ffcc, 0xff66ffcc, 0xff33ffcc, 0xff00ffcc, 0xffffcccc, 0xffcccccc, 0xff99cccc, 0xff66cccc, 0xff33cccc,
	0xff00cccc, 0xffff99cc, 0xffcc99cc, 0xff9999cc, 0xff6699cc, 0xff3399cc, 0xff0099cc, 0xffff66cc, 0xffcc66cc, 0xff9966cc, 0xff6666cc, 0xff3366cc, 0xff0066cc, 0xffff33cc, 0xffcc33cc, 0xff9933cc,
	0xff6633cc, 0xff3333cc, 0xff0033cc, 0xffff00cc, 0xffcc00cc, 0xff9900cc, 0xff6600cc, 0xff3300cc, 0xff0000cc, 0xffffff99, 0xffccff99, 0xff99ff99, 0xff66ff99, 0xff33ff99, 0xff00ff99, 0xffffcc99,
	0xffcccc99, 0xff99cc99, 0xff66cc99, 0xff33cc99, 0xff00cc99, 0xffff9999, 0xffcc9999, 0xff999999, 0xff669999, 0xff339999, 0xff009999, 0xffff6699, 0xffcc6699, 0xff996699, 0xff666699, 0xff336699,
	0xff006699, 0xffff3399, 0xffcc3399, 0xff993399, 0xff663399, 0xff333399, 0xff003399, 0xffff0099, 0xffcc0099, 0xff990099, 0xff660099, 0xff330099, 0xff000099, 0xffffff66, 0xffccff66, 0xff99ff66,
	0xff66ff66, 0xff33ff66, 0xff00ff66, 0xffffcc66, 0xffcccc66, 0xff99cc66, 0xff66cc66, 0xff33cc66, 0xff00cc66, 0xffff9966, 0xffcc9966, 0xff999966, 0xff669966, 0xff339966, 0xff009966, 0xffff6666,
	0xffcc6666, 0xff996666, 0xff666666, 0xff336666, 0xff006666, 0xffff3366, 0xffcc3366, 0xff993366, 0xff663366, 0xff333366, 0xff003366, 0xffff0066, 0xffcc0066, 0xff990066, 0xff660066, 0xff330066,
	0xff000066, 0xffffff33, 0xffccff33, 0xff99ff33, 0xff66ff33, 0xff33ff33, 0xff00ff33, 0xffffcc33, 0xffcccc33, 0xff99cc33, 0xff66cc33, 0xff33cc33, 0xff00cc33, 0xffff9933, 0xffcc9933, 0xff999933,
	0xff669933, 0xff339933, 0xff009933, 0xffff6633, 0xffcc6633, 0xff996633, 0xff666633, 0xff336633, 0xff006633, 0xffff3333, 0xffcc3333, 0xff993333, 0xff663333, 0xff333333, 0xff003333, 0xffff0033,
	0xffcc0033, 0xff990033, 0xff660033, 0xff330033, 0xff000033, 0xffffff00, 0xffccff00, 0xff99ff00, 0xff66ff00, 0xff33ff00, 0xff00ff00, 0xffffcc00, 0xffcccc00, 0xff99cc00, 0xff66cc00, 0xff33cc00,
	0xff00cc00, 0xffff9900, 0xffcc9900, 0xff999900, 0xff669900, 0xff339900, 0xff009900, 0xffff6600, 0xffcc6600, 0xff996600, 0xff666600, 0xff336600, 0xff006600, 0xffff3300, 0xffcc3300, 0xff993300,
	0xff663300, 0xff333300, 0xff003300, 0xffff0000, 0xffcc0000, 0xff990000, 0xff660000, 0xff330000, 0xff0000ee, 0xff0000dd, 0xff0000bb, 0xff0000aa, 0xff000088, 0xff000077, 0xff000055, 0xff000044,
	0xff000022, 0xff000011, 0xff00ee00, 0xff00dd00, 0xff00bb00, 0xff00aa00, 0xff008800, 0xff007700, 0xff005500, 0xff004400, 0xff002200, 0xff001100, 0xffee0000, 0xffdd0000, 0xffbb0000, 0xffaa0000,
	0xff880000, 0xff770000, 0xff550000, 0xff440000, 0xff220000, 0xff110000, 0xffeeeeee, 0xffdddddd, 0xffbbbbbb, 0xffaaaaaa, 0xff888888, 0xff777777, 0xff555555, 0xff444444, 0xff222222, 0xff111111
]

const MV_VERSION = 150

const ID_VOX  = 542658390 #MV_ID( 'V', 'O', 'X', ' ' )
const ID_MAIN = 1313423693 #MV_ID( 'M', 'A', 'I', 'N' )
const ID_SIZE = 1163544915 #MV_ID( 'S', 'I', 'Z', 'E' )
const ID_XYZI = 1230657880 #MV_ID( 'X', 'Y', 'Z', 'I' )
const ID_RGBA = 1094862674 #MV_ID( 'R', 'G', 'B', 'A' )

static func MV_ID(a, b, c, d ):
	return ( a ) | ( b << 8 ) | ( c << 16 ) | ( d << 24 )

export(String, FILE, "*.vox") var path = ""

class Voxel:
	var pos
	var color_idx

class Chunk:
	var id = 0
	var contentSize = 0
	var childrenSize = 0
	var end = 0

var version = 0
var size = Vector3()
var voxels = {} # Voxel
var palette = [] # Color[256]

func _ready():
	if !voxels.empty():
		return

	if _read_model_file(path):
		_create_voxels()

func _pos_to_vidx(pos):
	return (int(pos.x) << 16) | (int(pos.y) << 8) | int(pos.z)

func _cacl_skip_face(pos):
	var skip = 0
	var offset = [Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1),  Vector3(-1, 0, 0), Vector3(0, -1, 0),  Vector3(0, 0, -1)]
	for i in range(offset.size()):
		var idx = _pos_to_vidx(pos+offset[i])
		if voxels.has(idx):
			skip |= 1 << i
	return skip

func _create_voxel(data, color, offset, skip):
	var vertices = data.vertices
	var normals = data.normals
	var tangents = data.tangents
	var uvs = data.uvs

	var vtx_idx = 0;

	for i in range(6):
		
		if skip & (1 << i):
			continue
		
		var sig = 1
		if i >= 3:
			sig = -1

		var face_points = [Vector3(), Vector3(), Vector3(), Vector3()]
		var normal_points = [Vector3(), Vector3(), Vector3(), Vector3()];
		var uv_points = [ 0, 0, 0, 1, 1, 1, 1, 0 ]

		for j in range(4):

			var v = [0, 0, 0]
			v[0] = 1.0
			v[1] = 1 - 2 * ((j >> 1) & 1)
			v[2] = v[1] * (1 - 2 * (j & 1))

			for k in range(3): 

				if (i < 3):
					face_points[j][(i + k) % 3] = v[k] * sig
				else:
					face_points[3 - j][(i + k) % 3] = v[k] * sig
			
			normal_points[j] = Vector3()
			normal_points[j][i % 3] = sig

		#tri 1
		_ADD_VTX(0, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		_ADD_VTX(1, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		_ADD_VTX(2, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		#tri 2
		_ADD_VTX(2, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		_ADD_VTX(3, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		_ADD_VTX(0, vertices, normals, tangents, uvs, face_points, normal_points, uv_points)
		vtx_idx += 1
		
		for c in range(6):
			data.color.push_back(color)
			
	
	for i in range(vertices.size()-vtx_idx, vertices.size()):
		vertices[i] = vertices[i] * 0.5 + offset


func _ADD_VTX(idx, vertices, normals, tangents, uvs, face_points, normal_points, uv_points):
	vertices.push_back(face_points[idx])
	normals.push_back(normal_points[idx])
	tangents.push_back(normal_points[idx][1])
	tangents.push_back(normal_points[idx][2])
	tangents.push_back(normal_points[idx][0])
	tangents.push_back(1.0)
	uvs.push_back(Vector3(uv_points[idx * 2 + 0], uv_points[idx * 2 + 1], 0))

func _create_voxels():
	var mat = get_material_override()
	if mat == null:
		mat = FixedMaterial.new()
		mat.set_fixed_flag(FixedMaterial.FLAG_USE_COLOR_ARRAY, true)
		#mat.set_fixed_flag(FixedMaterial.FLAG_USE_ALPHA, true)
		set_material_override(mat)

	var mesh_data = {
		"vertices": [],
		"normals": [],
		"tangents": [],
		"uvs": [],
		"color": [],
	}

	var keys = voxels.keys()
	for k in keys:
		var voxel = voxels[k]
		var offset = voxel.pos - size*0.5
		var color = palette[voxel.color_idx-1]
		var skip = _cacl_skip_face(voxel.pos)
		_create_voxel(mesh_data, color, offset, skip)

	var arr = []
	arr.resize(9)
	arr[Mesh.ARRAY_VERTEX] = Vector3Array(mesh_data.vertices)
	arr[Mesh.ARRAY_NORMAL] = Vector3Array(mesh_data.normals)
	arr[Mesh.ARRAY_TANGENT] = FloatArray(mesh_data.tangents)
	arr[Mesh.ARRAY_COLOR] = ColorArray(mesh_data.color)

	var mesh = Mesh.new()
	mesh.add_surface(Mesh.PRIMITIVE_TRIANGLES, arr)
	set_mesh(mesh)

func _readVec3(f):
	var v = Vector3()
	v.x = float(f.get_32())
	v.z = float(f.get_32())
	v.y = float(f.get_32())
	return v

func _read_voxel(f):
	var v = Voxel.new()
	v.pos = Vector3()
	v.pos.x = float(f.get_8())
	v.pos.z = float(f.get_8())
	v.pos.y = float(f.get_8())
	v.color_idx = int(f.get_8())
	return v

func _read_model_file(path):
	var file = File.new()
	if file.open(path, File.READ):
		print( "error : cannot open file \"%s\"" % path )
		return false

	# magic number
	var magic = file.get_32()
	if ( magic != ID_VOX ):
		print( "error : magic number does not match : %d != %d" % [magic, ID_VOX])
		return false

	# version
	version = file.get_32()
	if ( version != MV_VERSION ):
		printf( "error : version does not match : %d != %d" % [version, MV_VERSION] )
		return false

	# copy default palette
#	palette = Array(mv_default_palette)
	for i in range(256):
		var c = Color(mv_default_palette[i])
		#print("%X%X%X%X" % [c.r8, c.g8, c.b8, c.a8])
		palette.push_back(Color(c.a, c.b, c.g, c.r))

	# main chunk
	var mainChunk = _read_chunk(file)
	if ( mainChunk.id != ID_MAIN ):
		print( "error : MAIN chunk is not found" )
		return false

	# skip content of main chunk
	file.seek( mainChunk.contentSize + file.get_pos())

	# read children chunks
	while ( file.get_pos() < mainChunk.end ):
		# read chunk header
		var sub = _read_chunk(file)

		if ( sub.id == ID_SIZE ):
			# size
			size = _readVec3(file)
			if ( size.x <= 0 || size.y <= 0 || size.z <= 0 ):
				print( "error : invalid size : %f %f %f" % [size.x, size.y, size.z] )
				return false

		elif ( sub.id == ID_XYZI ):
			# numVoxels
			var numVoxels = file.get_32()
			if ( numVoxels < 0 ):
				print( "error : negative number of voxels : %d" % numVoxels )
				return false

			# voxels
			if ( numVoxels > 0 ):
				voxels = {}
				#voxels.resize( numVoxels )
				for i in range(numVoxels):
					#voxels[i] = _read_voxel(file)
					var v = _read_voxel(file)
					var idx = (int(v.pos.x) << 16) | (int(v.pos.y) << 8) | int(v.pos.z)
					voxels[idx] = v
				#file.ReadData( voxels.data(), sizeof( MV_Voxel ), numVoxels )

		elif ( sub.id == ID_RGBA ):
			# NOTICE : the color 0~254 are mapped to the palette 1~255
			for i in range(0, 256):
				var r = float(file.get_8()) / 255
				var g = float(file.get_8()) / 255
				var b = float(file.get_8()) / 255
				var a = float(file.get_8()) / 255
				palette[i] = Color(r, g, b, a)
			#file.ReadData( palette + 1, sizeof( MV_RGBA ), 255 )

			# NOTICE : skip the last unused color
			#MV_RGBA unused;
			#file.ReadData( &unused, sizeof( MV_RGBA ), 1 )
#			file.get_32()

		# skip unread bytes of current chunk or the whole unused chunk
		file.seek(sub.end)

	# print model info
	print( "log : model : size %d %d %d : numVoxels %d" % [size.x, size.y, size.z, voxels.size()] )

	return true;

func _read_chunk(file):
	# read chunk header
	var chunk = Chunk.new()
	chunk.id = file.get_32()
	chunk.contentSize  = file.get_32()
	chunk.childrenSize = file.get_32()

	# end of chunk : used for skipping the whole chunk
	chunk.end = file.get_pos() + chunk.contentSize + chunk.childrenSize

	# print chunk info
	var id_2_str = {ID_VOX: 'VOX ', 
		ID_MAIN: 'MAIN',
		ID_SIZE: 'SIZE',
		ID_XYZI: 'XYZI',
		ID_RGBA: 'RGBA',
	}
	
	var sid = str(chunk.id)
	if id_2_str.has(chunk.id):
		sid = id_2_str[chunk.id]

	print( "log : chunk : %s : %d %d" % [sid, chunk.contentSize, chunk.childrenSize])
#	const char *c = ( const char * )( &chunk.id )
#	print( "log : chunk : %c%c%c%c : %d %d\n" % c[0], c[1], c[2], c[3], chunk.contentSize, chunk.childrenSize)
	return chunk