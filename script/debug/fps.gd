extends Label

var elapsed_time = 1.0

func _on_update_fps():
	var msg = "FPS: %d" % Performance.get_monitor(Performance.TIME_FPS)
	set_text(msg)

func _ready():
	set_fixed_process(true)
	
func _fixed_process(dt):
	elapsed_time += dt
	if elapsed_time >= 1:
		_on_update_fps()
		elapsed_time -= 1.0